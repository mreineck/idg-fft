#include <iostream>
#include <iomanip>
#include <complex>

#define SIZE 128

/********************************************************************************
 * FFTW
 ********************************************************************************/
#define FFTW_FLAGS FFTW_ESTIMATE

/********************************************************************************
 * Debug
 ********************************************************************************/
#define PRINT_DATA 0
#define PRINT_ERRORS 1

template<typename T>
void print(int height, int width, T* data)
{
  #if PRINT_DATA
  for (int y = 0; y < height; y++)
  {
    for (int x = 0; x < width; x++)
    {
      std::cout << std::setprecision(2) << data[y * width + x] << "\t";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
  #endif
}

template<typename T>
void print(int n, T* data)
{
  #if PRINT_DATA
  for (int x = 0; x < n; x++)
  {
    std::cout << std::setprecision(2) << data[x] << "\t";
  }
  std::cout << std::endl;
  #endif
}


/********************************************************************************
 * Get accuracy
 ********************************************************************************/
float get_accuracy(const idg::Array2D<std::complex<float>>& A,
                   const idg::Array2D<std::complex<float>>& B) {
  int height = A.get_y_dim();
  int width = B.get_x_dim();
  assert(height == B.get_y_dim());
  assert(width == B.get_x_dim());

  double r_error = 0.0;
  double i_error = 0.0;
  int nnz = 0;

  float r_max = 1;
  float i_max = 1;
  for (int i = 0; i < height*width; i++) {
    float r_value = abs(A(0, i).real());
    float i_value = abs(A(0, i).imag());
    if (r_value > r_max) {
      r_max = r_value;
    }
    if (i_value > i_max) {
      i_max = i_value;
    }
  }

#if defined(PRINT_ERRORS)
  int nerrors = 0;
#endif

  for (int i = 0; i < height*width; i++) {
    float r_cmp = A(0, i).real();
    float i_cmp = A(0, i).imag();
    float r_ref = B(0, i).real();
    float i_ref = B(0, i).imag();
    double r_diff = r_ref - r_cmp;
    double i_diff = i_ref - i_cmp;
    if (abs(A(0, i)) != 0 || abs(B(0, i)) != 0) {
#if defined(PRINT_ERRORS)
      if ((abs(r_diff) || abs(i_diff)) && nerrors < 16) {
        printf("%d: (%f, %f) - (%f, %f) = (%f, %f)\n", i, r_cmp, i_cmp, r_ref, i_ref,
               r_diff, i_diff);
        nerrors++;
      }
#endif
      nnz++;
      r_error += (r_diff * r_diff) / r_max;
      i_error += (i_diff * i_diff) / i_max;
    }
  }

  printf("r_error: %f\n", r_error);
  printf("i_error: %f\n", i_error);
  printf("nnz: %d\n", nnz);

  r_error /= std::max(1, nnz);
  i_error /= std::max(1, nnz);

  return sqrt(r_error + i_error);
}

float get_accuracy(const idg::Grid& A,
                   const idg::Grid& B) {
  assert(A.get_w_dim() == 1);
  assert(A.get_z_dim() == 1);
  assert(B.get_w_dim() == 1);
  assert(B.get_z_dim() == 1);
  idg::Array2D<std::complex<float>> A_(A.data(), A.get_y_dim(), A.get_x_dim());
  idg::Array2D<std::complex<float>> B_(B.data(), B.get_y_dim(), B.get_x_dim());
  return get_accuracy(A_, B_);
}

float get_accuracy(const idg::Array2D<float>& A,
                   const idg::Array2D<float>& B) {
  int height = A.get_y_dim();
  int width = B.get_x_dim();
  assert(height == B.get_y_dim());
  assert(width == B.get_x_dim());

  double error = 0.0;
  int nnz = 0;

  float max = 1;
  for (int i = 0; i < height*width; i++) {
    float value = abs(A(0, i));
    if (value > max) {
      max = value;
    }
  }

#if defined(PRINT_ERRORS)
  int nerrors = 0;
#endif

  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      float cmp = A(y, x);
      float ref = B(y, x);
      double diff = ref - cmp;
      if ((A(y, x) != 0) || B(y, x) != 0) {
  #if defined(PRINT_ERRORS)
        if (abs(diff) > 0 && nerrors < 16) {
          int i = y * width + x;
          printf("%d: %f - %f = %f\n", i, cmp, ref, cmp - ref);
          nerrors++;
        }
  #endif
        nnz++;
        error += (diff * diff) / max;
      }
    }
  }

  printf("error: %f\n", error);
  printf("nnz: %d\n", nnz);

  error /= std::max(1, nnz);

  return sqrt(error);
}


/********************************************************************************
 * Init data
 ********************************************************************************/
void init_data(idg::Array2D<float>& data)
{
  for (int y = 0; y < data.get_y_dim(); y++)
  {
    for (int x = 0; x < data.get_x_dim(); x++)
    {
      float a = (float) (1+y) / data.get_y_dim();
      float b = (float) (1+x) / data.get_x_dim();
      data(y, x) = a + b / 10;
    }
  }
}

void init_data(idg::Array2D<std::complex<float>>& data)
{
  for (int y = 0; y < data.get_y_dim(); y++)
  {
    for (int x = 0; x < data.get_x_dim(); x++)
    {
      float real = (float) (1+y) / data.get_y_dim();
      float imag = (float) (1+x) / data.get_x_dim();
      data(y, x) = std::complex<float>(real, imag);
      data(y, x) = std::complex<float>(y, x);
    }
  }
}


/********************************************************************************
 * LIKWID
 ********************************************************************************/
#ifdef LIKWID_PERFMON
#include <likwid.h>
#else
#define LIKWID_MARKER_INIT
#define LIKWID_MARKER_THREADINIT
#define LIKWID_MARKER_SWITCH
#define LIKWID_MARKER_REGISTER(regionTag)
#define LIKWID_MARKER_START(regionTag)
#define LIKWID_MARKER_STOP(regionTag)
#define LIKWID_MARKER_CLOSE
#define LIKWID_MARKER_GET(regionTag, nevents, events, time, count)
#endif
