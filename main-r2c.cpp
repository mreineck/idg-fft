#include <fftw3.h>
#include <idg.h>

#include "helper.h"

void fft2f_r2c(int imgHeight, int imgWidth, float *in, std::complex<float> *out)
{
  const size_t imgSize = imgWidth * imgHeight;
  const size_t complexSize = (imgWidth / 2 + 1) * imgHeight;

  fftwf_complex* temp = fftwf_alloc_complex(complexSize);

  fftwf_plan_with_nthreads(omp_get_max_threads());
  fftwf_plan plan_r2c = fftwf_plan_dft_r2c_2d(imgHeight, imgWidth, in, temp, FFTW_FLAGS);

  LIKWID_MARKER_REGISTER("fft");
  LIKWID_MARKER_START("fft");
  fftwf_execute_dft_r2c(plan_r2c, in, temp);

  float fact = 1.0 / imgSize;
  for (size_t i = 0; i != complexSize; ++i)
    out[i] = reinterpret_cast<std::complex<float>*>(temp)[i] * fact;

  LIKWID_MARKER_STOP("fft");

  fftwf_free(temp);
  fftwf_destroy_plan(plan_r2c);
  fftwf_cleanup_threads();
}

void fft2f_r2c_composite(int imgHeight, int imgWidth, float *in, std::complex<float> *out, bool scale = true)
{
  assert(imgHeight == imgWidth);
  const size_t imgSize = imgWidth * imgHeight;
  const size_t complexWidth = imgWidth / 2 + 1;
  const size_t complexSize = imgHeight * complexWidth;

  fftwf_complex* temp1 = fftwf_alloc_complex(complexSize);
  std::complex<float>* temp1_ptr = reinterpret_cast<std::complex<float>*>(temp1);

  fftwf_plan_with_nthreads(1);
  fftwf_plan plan_r2c_row = fftwf_plan_dft_r2c_1d(imgWidth, nullptr, nullptr, FFTW_FLAGS);
  fftwf_plan plan_c2c_col = fftwf_plan_dft_1d(imgHeight, nullptr, nullptr, FFTW_FORWARD, FFTW_FLAGS);
  LIKWID_MARKER_REGISTER("fft-composite");
  LIKWID_MARKER_START("fft-composite");

  #pragma omp parallel for
  for (size_t y = 0; y < imgHeight; y++)
  {
    fftwf_execute_dft_r2c(plan_r2c_row, &in[y * imgWidth], &temp1[y * complexWidth]);
  }

  #pragma omp parallel for
  for (size_t x = 0; x < complexWidth; x++)
  {
    fftwf_complex* temp2 = fftwf_alloc_complex(imgHeight);
    std::complex<float>* temp2_ptr = reinterpret_cast<std::complex<float>*>(temp2);

    for (size_t y = 0; y < imgHeight; y++)
    {
      temp2_ptr[y] = temp1_ptr[y * complexWidth + x];
    }

    fftwf_execute_dft(plan_c2c_col, temp2, temp2);

    for (size_t y = 0; y < imgHeight; y++)
    {
      if (scale) {
        float fact = 1.0 / imgSize;
        temp2_ptr[y] *= fact;
      }
      out[y * complexWidth + x] = temp2_ptr[y];
    }

    fftwf_free(temp2);
  }

  LIKWID_MARKER_STOP("fft-composite");

  fftwf_free(temp1);
  fftwf_destroy_plan(plan_r2c_row);
  fftwf_destroy_plan(plan_c2c_col);
  fftwf_cleanup_threads();
}

void test_r2c(int size)
{
  int size2 = size / 2 + 1;
  std::cout << "size: " << size << ", complex size: " << size2 << std::endl;
  
  std::cout << "input" << std::endl;
  idg::Array2D<float> in(size, size);
  init_data(in);
  
  double runtime_fft;

  // Reference
  idg::Array2D<std::complex<float>> out_reference(size, size2);
  std::cout << "fft2f_r2c" << std::endl;
  fft2f_r2c(size, size, in.data(), out_reference.data());
  print(size, size2, out_reference.data());

  // Composite
  std::cout << "fft2f_r2c_composite" << std::endl;
  idg::Array2D<std::complex<float>> out_composite(size, size2);
  out_composite.zero();
  runtime_fft = -omp_get_wtime();
  fft2f_r2c_composite(size, size, in.data(), out_composite.data());
  print(size, size2, out_composite.data());

  get_accuracy(out_reference, out_composite);
  std::cout << std::endl;
}

int main(int argc, char **argv)
{
  LIKWID_MARKER_INIT;

  test_r2c(SIZE);
  test_r2c(SIZE*2);
  test_r2c(SIZE+1);

  LIKWID_MARKER_CLOSE;
}
