CXX=g++
CXXFLAGS=-march=native -O3 -fopenmp
IDGFLAGS=-I${IDG_INCLUDE} -L${IDG_LIB} -lidg-common -lidg-fft
FFTWFLAGS=-I${FFTW_INC} -L${FFTW_LIB} -lfftw3f -lfftw3f_omp
LIKWIDFLAGS=-DLIKWID_PERFMON -I${LIKWID_INC} -L${LIKWID_LIB} -llikwid

default: main-c2c.x main-r2c.x main-c2r.x main-r2r.x

%.x: %.cpp
	${CXX} -o $@ $^ ${CXXFLAGS} ${IDGFLAGS} ${FFTWFLAGS} ${LIKWIDFLAGS}
